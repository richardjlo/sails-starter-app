# Sails-starter-app

A [Sails.js](http://sailsjs.org/#!) starter application that is ready for deployment on Heroku.

Includes:

* Sails.js (0.9.4)
* EJS (0.8.4)
* Bootstrap (3.0.0)
* jQuery (1.10.2)

### Deploy with Heroku

1. Sign up for Heroku: https://devcenter.heroku.com/articles/quickstart


2. Download project and init git repository
```bash
git init && git add . && git commit
```

3. Create Heroku project
```bash
heroku create
```

4. Deploy latest commit to Heroku
```bash
git push heroku master
```

5. Scale up to 1 Heroku dyno 
```bash
heroku ps:scale web=1
```

6. Open Heroku app in browser
```bash
heroku open
```

### Sources

* https://devcenter.heroku.com/articles/getting-started-with-nodejs 
* http://dennisrongo.com/blog/2013/07/05/deploying-sails-dot-js-to-heroku/

* https://github.com/balderdashy/sails/wiki/configuration#wiki-application.js
* http://irlnathan.github.io/sailscasts/blog/2013/08/20/building-a-sails-application-ep1-installing-sails-and-create-initial-project/ 


### Licence

[MIT License](http://opensource.org/licenses/mit-license.php).

Copyright [@richardjlo](http://twitter.com/richardjlo)

    

